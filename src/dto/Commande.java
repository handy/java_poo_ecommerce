package dto;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class Commande implements Serializable {

    private final static long serialVersionUID = 1L;

    private String[] itemIds;
    private int nCommande;
    private Date dateDAchat;
    private Client client;
    private boolean state = false;

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public int getnCommande() {
        return nCommande;
    }

    public void setnCommande(int nCommande) {
        this.nCommande = nCommande;
    }

    public Date getDateDAchat() {
        return dateDAchat;
    }

    public void setDateDAchat(Date dateDAchat) {
        this.dateDAchat = dateDAchat;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public Commande(String[] itemIds, int nCommande, Date dateDAchat, Client client, boolean state) {
        this.itemIds = itemIds;
        this.nCommande = nCommande;
        this.dateDAchat = dateDAchat;
        this.client = client;
        this.state = state;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "itemIds=" + Arrays.toString(itemIds) +
                ", nCommande=" + nCommande +
                ", dateDAchat=" + dateDAchat +
                ", client=" + client +
                ", state=" + state +
                '}';
    }
}
