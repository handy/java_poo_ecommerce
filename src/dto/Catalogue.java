package dto;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Catalogue implements Serializable {
    private final static long serialVersionUID = 1L;

    private Map<String, Produit>produits = new HashMap<>();

    synchronized public Map<String, Produit> getProduits() {
        return Collections.unmodifiableMap(produits);
    }

    synchronized public void setProduits(String nProduit, Produit produit){
        produits.put(nProduit, produit);
    }

    public Catalogue(Map<String, Produit> produits) {
        this.produits = produits;
    }

    @Override
    public String toString() {
        return "Catalogue{" +
                "produits=" + produits +
                '}';
    }
}
