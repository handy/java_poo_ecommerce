package dto;

import java.io.Serializable;

public class Client implements Serializable {

    private final static long serialVersionUID = 1L;

    private String nom;
    private String password;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Client(String nom, String password) {
        this.nom = nom;
        this.password = password;
    }

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
