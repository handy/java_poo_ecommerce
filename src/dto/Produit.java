package dto;

import java.io.Serializable;

public class Produit implements Serializable {

    private final static long serialVersionUID = 1L;

    String nProduit;
    String descriptif;
    double prix;
    boolean disponible;
    int stock;

    public String getnProduit() {
        return nProduit;
    }

    public void setnProduit(String nProduit) {
        this.nProduit = nProduit;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Produit(String nProduit, String descriptif, double prix, boolean disponible, int stock) {
        this.nProduit = nProduit;
        this.descriptif = descriptif;
        this.prix = prix;
        this.disponible = disponible;
        this.stock = stock;
    }

    @Override
    public String toString() {
        return "Produit{" +
                "nProduit=" + nProduit +
                ", descriptif='" + descriptif + '\'' +
                ", prix=" + prix +
                ", disponible='" + disponible + '\'' +
                ", stock=" + stock +
                '}';
    }
}
